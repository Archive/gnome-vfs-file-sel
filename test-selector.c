#include <string.h>

#include <gnome.h>

#include "file-selector.h"
#include "config.h"


int main (int argc, char *argv[]) {
  char *mime_types[] = { "image/x-png",
			 "image/jpeg",
			 NULL };
  gnome_init ("File Dialogue Tester", VERSION, argc, argv);

  file_selector_select (FILE_SELECTOR_OPEN, mime_types);

  gtk_main ();

  return 0;
}
