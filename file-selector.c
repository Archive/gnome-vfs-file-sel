/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 2 -*- */
/*
 * file-selector.c: A file selector widget
 *
 * Author:
 *   Seth Nickell <snickell@stanford.edu>
 *
 * (C) 2001 Seth Nickell
 */

#include "config.h"

#include "file-selector.h"

#include <time.h>
#include <string.h>

#include <gnome.h>
#include <glade/glade.h>

#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>

#include <gal/e-table/e-tree-scrolled.h>
#include <gal/e-table/e-tree.h>
#include <gal/shortcut-bar/e-shortcut-bar.h>
#include "gnome-vfs-tree-model.h"
#include <libnautilus-extensions/nautilus-global-preferences.h>

GnomeVFSURI *current_location;

GtkWidget *dialogue;
GnomeVFSTreeModel *model;
ETreeScrolled *tree_scrolled;
ETree *tree;
EShortcutBar *shortcut_bar;

GtkEntry  *location;
GtkButton *parent_button;

GdkColor dark, light;
gboolean is_dark;

static void
load_directory (GnomeVFSURI *uri, GtkEntry *location, GnomeVFSTreeModel *model) 
{
  char *uri_text, *displayable;
  GnomeVFSURI *parent_uri;

  /* If this URI has no parent, it is the root node of this method/host combo
     which means we should disable the "up directory" button */
  parent_uri = gnome_vfs_uri_get_parent (uri);
  if (parent_uri == NULL) {
    gtk_widget_set_sensitive (GTK_WIDGET (parent_button), FALSE);
  } else {
    gtk_widget_set_sensitive (GTK_WIDGET (parent_button), TRUE);
  }

  gnome_vfs_uri_unref (parent_uri);

  if (current_location) {
    gnome_vfs_uri_unref (current_location);
  }

  gnome_vfs_uri_ref (uri);
  current_location = uri;

  /* Turn the URI into a string and use a gnome-vfs-util.c function to
     unescape it in a way that displays nicely */
  uri_text = gnome_vfs_uri_to_string (uri, GNOME_VFS_URI_HIDE_NONE);
  displayable = gnome_vfs_unescape_string_for_display (uri_text);

  /* Display the text into the location bar */
  gtk_entry_set_text (location, displayable);

  g_free (uri_text);
  g_free (displayable);

  gnome_vfs_tree_model_change_uri(model, uri);
}


static void
activate_entry (GtkWidget *widget, guint row, ETreePath path, guint column, GdkEventButton *event)
{
  GnomeVFSURI *new_uri;
  GnomeVFSFileInfo info;
  GnomeVFSResult result;
  GnomeVFSFileInfoOptions info_options = GNOME_VFS_FILE_INFO_GET_MIME_TYPE;

  new_uri = gnome_vfs_tree_model_node_get_uri(model, path);

  /* Append the item they double clicked on to the current URI and reload */
  result = gnome_vfs_get_file_info_uri (new_uri, &info, info_options);
  if (result == GNOME_VFS_OK) {
    if (strcmp (info.mime_type, "x-directory/normal") == 0) {
      /* FIXME: check if its a directory, and if it is do a load_directory
	 else we use this file as "the one" */
      load_directory (new_uri, (GtkEntry *) location, model);
    }
  }

  gnome_vfs_uri_unref (new_uri);
}

static void
cancel_button_click_cb (GtkWidget *widget, gpointer data)
{
  exit (0);
}

/* Button clicked to go up a directory */
static void
parent_button_click_cb (GtkWidget *widget, gpointer data)
{
  GnomeVFSURI *new_uri;

  /* Get parent returns the URI one level "up" in the filesystem */
  new_uri = gnome_vfs_uri_get_parent (current_location);
  if (new_uri != NULL) {
    load_directory (new_uri, location, model);
  }
  gnome_vfs_uri_unref (new_uri);
}

/* Call load again on the current URI, reloads the directory */
static void
refresh_button_click_cb (GtkWidget *widget, gpointer data)
{
  GnomeVFSURI *new_uri;

  new_uri = gnome_vfs_uri_dup (current_location);
  load_directory (new_uri, location, model);
}

static GnomeVFSURI *
get_location_bar_uri (GtkEntry *location_bar) 
{
  GnomeVFSURI *uri;
  char *text, *uri_text;

  text = gtk_entry_get_text (location_bar);
  uri_text = gnome_vfs_escape_host_and_path_string (text);
  uri = gnome_vfs_uri_new (uri_text);

  return uri;
}

static void
location_bar_changed_cb (GtkEditable *editable, gpointer data)
{
  GnomeVFSURI *uri;
  GtkLabel *location_label;

  uri = get_location_bar_uri (GTK_ENTRY (editable));

  location_label = GTK_LABEL (data);

  if (gnome_vfs_uri_equal (uri, current_location)) {
    gtk_label_set_text (location_label, "Location:");
  } else {
    gtk_label_set_text (location_label, "Goto:");
  }
}

static void
location_bar_activate_cb (GtkEditable *editable, gpointer data)
{
  GnomeVFSURI *uri;
  uri = get_location_bar_uri (GTK_ENTRY (editable));
  load_directory (uri, location, model);
}

static GtkMenu 
*create_menu_for_file_types (char *mime_types[], GtkMenu *menu) 
{
  //GtkMenu *menu;
  const char *description;
  int i;

  menu = GTK_MENU (gtk_menu_new());
  
  for (i=0; mime_types[i] != NULL; i++) {
    description = gnome_vfs_mime_get_description (mime_types[i]);
    printf ("description is %s\n", description);
    if (description != NULL) {
      gtk_menu_append (menu, gtk_menu_item_new_with_label (description));
    }
  }

  return menu;
}

static void
etree_destroy(ETree *etree, gpointer data)
{
  e_tree_save_expanded_state(etree, "expanded-state");
}

/* ETable creation */
#define SPEC "<ETableSpecification cursor-mode=\"line\" draw-grid=\"false\" selection-mode=\"browse\">" \
	     "<ETableColumn model_col= \"0\" _title=\"Name\" expansion=\"1.0\" minimum_width=\"18\" resizable=\"true\" cell=\"tree-string\" compare=\"string\"/>" \
	     "<ETableColumn model_col= \"1\" _title=\"Last Changed\" expansion=\"1.0\" minimum_width=\"18\" resizable=\"true\" cell=\"date\" compare=\"integer\"/>" \
             "<ETableState> <column source=\"0\"/> <column source=\"1\"/> <grouping> </grouping> </ETableState>" \
	     "</ETableSpecification>"

GtkWidget *
create_e_tree (gchar *name, gchar *string1, 
	       gchar *string2, gint int1, gint int2);
GtkWidget *
create_shortcut_bar (gchar *name, gchar *string1,
		     gchar *string2, gint int1, gint int2);

GtkWidget *
create_e_tree (gchar *name, gchar *string1, 
	       gchar *string2, gint int1, gint int2)
{
  GtkWidget *tree;
  GnomeVFSTreeModel *model;
  ETree *etree;

  model = gnome_vfs_tree_model_new(NULL);
  tree = e_tree_scrolled_new (E_TREE_MODEL(model), NULL, SPEC, NULL);
  gtk_object_set_data (GTK_OBJECT(tree), "FileSelector::model", model);
  etree = e_tree_scrolled_get_tree(E_TREE_SCROLLED(tree));
  e_tree_root_node_set_visible(etree, FALSE);
  e_tree_load_expanded_state(etree, "expanded-state");
  gtk_signal_connect (GTK_OBJECT(etree), "destroy",
		      GTK_SIGNAL_FUNC(etree_destroy), etree);
  return tree;
}

GtkWidget *
create_shortcut_bar (gchar *name, gchar *string1,
		     gchar *string2, gint int1, gint int2)
{
  GtkWidget *shortcut_bar;
  EShortcutModel *shortcut_model;
  gint group_num;

  shortcut_model = e_shortcut_model_new ();
  shortcut_bar = e_shortcut_bar_new ();
  e_shortcut_bar_set_model (E_SHORTCUT_BAR (shortcut_bar),
			    shortcut_model);
  group_num = e_shortcut_model_add_group (shortcut_model, -1, "Shortcuts");
  e_shortcut_bar_set_view_type (E_SHORTCUT_BAR (shortcut_bar), group_num, E_ICON_BAR_SMALL_ICONS);

  //e_shortcut_model_add_item (shortcut_model, group_name, -1, 

  return GTK_WIDGET (shortcut_bar);
}

void 
file_selector_select (FileSelectorType type, char *mime_types[])
{
  GladeXML *xml;
  GtkWidget *toolbar;
  GtkButton *refresh_button, *cancel_button;
  GtkOptionMenu *file_types;
  GtkMenu *file_types_menu;
  GtkLabel  *location_label;
  GnomeVFSURI *home_dir_uri;
  char *home_dir, *home_dir_uri_text;

  // gdk_color_parse ("999999", &dark);
  // gdk_color_parse ("BBBBBB", &light);
  dark.red = 60000;
  dark.green = 60000;
  dark.blue = 60000;

  light.red = 62000;
  light.green = 62000;
  light.blue = 62000;

  is_dark = FALSE;

  glade_gnome_init ();
  gnome_vfs_init ();
  nautilus_global_preferences_initialize ();

  /* Load the user interface, get direct access to particular widgets */
  xml = glade_xml_new ("file-selector.glade", "file_selector");

  dialogue = glade_xml_get_widget (xml, "file_selector");
  toolbar = glade_xml_get_widget (xml, "nav_toolbar");
  location = GTK_ENTRY (glade_xml_get_widget (xml, "location_entry"));
  parent_button = GTK_BUTTON (glade_xml_get_widget (xml, "parent_button"));
  refresh_button = GTK_BUTTON (glade_xml_get_widget (xml, "refresh_button"));
  cancel_button = GTK_BUTTON (glade_xml_get_widget (xml, "cancel_button"));
  location_label = GTK_LABEL (glade_xml_get_widget (xml, "location_label"));
  file_types = GTK_OPTION_MENU (glade_xml_get_widget (xml, "file_types"));
  shortcut_bar = E_SHORTCUT_BAR (glade_xml_get_widget (xml, "shortcut_bar"));

  tree_scrolled = E_TREE_SCROLLED (glade_xml_get_widget (xml, "list_view"));
  tree = e_tree_scrolled_get_tree(tree_scrolled);
  model = gtk_object_get_data(GTK_OBJECT(tree_scrolled), "FileSelector::model");
  

  /* Menu stuff for file types */
  file_types_menu = GTK_MENU (gtk_option_menu_get_menu (file_types));
  file_types_menu = create_menu_for_file_types (mime_types, file_types_menu);

  /* Customize specific widgets in ways Glade does not allow us to */
  gtk_toolbar_set_style ((GtkToolbar *) toolbar, GTK_TOOLBAR_ICONS);

  /* Connect Signals */
  gtk_signal_connect (GTK_OBJECT (tree), "double_click",
		      GTK_SIGNAL_FUNC (activate_entry), NULL);
  gtk_signal_connect (GTK_OBJECT (parent_button),
		      "clicked",
		      GTK_SIGNAL_FUNC (parent_button_click_cb),
		      NULL);
  gtk_signal_connect (GTK_OBJECT (refresh_button),
		      "clicked",
		      GTK_SIGNAL_FUNC (refresh_button_click_cb),
		      NULL);
  gtk_signal_connect (GTK_OBJECT (location),
		      "changed",
		      GTK_SIGNAL_FUNC (location_bar_changed_cb),
		      location_label);
  gtk_signal_connect (GTK_OBJECT (location), "activate",
		      GTK_SIGNAL_FUNC (location_bar_activate_cb),
		      NULL);
  gtk_signal_connect (GTK_OBJECT (cancel_button),
		      "clicked",
		      GTK_SIGNAL_FUNC (cancel_button_click_cb),
		      NULL);

  /* Get a URI to the users home directory */
  home_dir = g_get_home_dir();
  home_dir_uri_text = malloc (strlen ("file://") + strlen (home_dir) + 1);
  sprintf (home_dir_uri_text, "%s%s", "file://", home_dir);

  home_dir_uri = gnome_vfs_uri_new (home_dir_uri_text);

  /* Start the dialogue in the home directory */
  load_directory (home_dir_uri, location, model);

  gnome_vfs_uri_unref (home_dir_uri);
}
