/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * gnome-vfs-tree-model.c: a Tree Model implementation that the programmer.
 *
 * Author:
 *   Chris Lahey <clahey@ximian.com>
 *   Seth Nickell <snickell@stanford.edu>
 *
 * Adapted from the gtree code and ETableModel.
 *
 * (C) 2000, 2001 Ximian, Inc.
 */

#include <config.h>

#include "gnome-vfs-tree-model.h"

#include <gal/util/e-i18n.h>

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include <gtk/gtksignal.h>
#include <gal/util/e-util.h>
#include <gal/util/e-xml-utils.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>
#include <libnautilus/nautilus-icon-factory.h>
#include <libnautilus/nautilus-file.h>

#define ROW_HEIGHT 20

enum {
	COL_FILE_NAME,
	COL_DATE_MODIFIED,

	LAST_COL
};

#define PARENT_TYPE E_TREE_MODEL_TYPE

#define TREEPATH_CHUNK_AREA_SIZE (30 * sizeof (GnomeVFSTreeModelPath))

#define d(x)

static ETreeModel *parent_class;
static GMemChunk  *node_chunk;

typedef struct GnomeVFSTreeModelPath GnomeVFSTreeModelPath;

struct GnomeVFSTreeModelPath {
	/* parent/child/sibling pointers */
	GnomeVFSTreeModelPath  *parent;
	gint                    num_children;
	GnomeVFSTreeModelPath **children;
	int                     position;

	GnomeVFSURI            *uri;
	GnomeVFSFileInfo       *info;
	GdkPixbuf              *pixbuf;
};

struct GnomeVFSTreeModelPriv {
	GnomeVFSURI *uri;
	GnomeVFSTreeModelPath *root;
};

enum {
	ARG_0,
};

static void free_path (GnomeVFSTreeModelPath *path);



/* idle callbacks */



/* Helper functions */

static char *
filter_date (time_t date)
{
	time_t nowdate = time (NULL);
	time_t yesdate;
	struct tm then, now, yesterday;
	char buf[26];
	gboolean done = FALSE;

	if (date == 0)
		return g_strdup (_("?"));

	localtime_r (&date, &then);
	localtime_r (&nowdate, &now);
	if (then.tm_mday == now.tm_mday &&
	    then.tm_mon == now.tm_mon &&
	    then.tm_year == now.tm_year) {
		strftime (buf, 26, _("Today %l:%M %p"), &then);
		done = TRUE;
	}
	if (!done) {
		yesdate = nowdate - 60 * 60 * 24;
		localtime_r (&yesdate, &yesterday);
		if (then.tm_mday == yesterday.tm_mday &&
		    then.tm_mon == yesterday.tm_mon &&
		    then.tm_year == yesterday.tm_year) {
			strftime (buf, 26, _("Yesterday %l:%M %p"), &then);
			done = TRUE;
		}
	}
	if (!done) {
		int i;
		for (i = 2; i < 7; i++) {
			yesdate = nowdate - 60 * 60 * 24 * i;
			localtime_r (&yesdate, &yesterday);
			if (then.tm_mday == yesterday.tm_mday &&
			    then.tm_mon == yesterday.tm_mon &&
			    then.tm_year == yesterday.tm_year) {
				strftime (buf, 26, _("%a %l:%M %p"), &then);
				done = TRUE;
				break;
			}
		}
	}
	if (!done) {
		if (then.tm_year == now.tm_year) {
			strftime (buf, 26, _("%b %d %l:%M %p"), &then);
		} else {
			strftime (buf, 26, _("%b %d %Y"), &then);
		}
	}
#if 0
#ifdef CTIME_R_THREE_ARGS
	ctime_r (&date, buf, 26);
#else
	ctime_r (&date, buf);
#endif
#endif

	return g_strdup (buf);
}

static void
free_children (GnomeVFSTreeModelPath *path)
{
	int i;

	if (path == NULL)
		return;

	for (i = 0; i < path->num_children; i++) {
		free_path (path->children[i]);
	}

	g_free (path->children);
	path->children = NULL;
	path->num_children = -1;
}

static void
free_path (GnomeVFSTreeModelPath *path)
{
	if (path == NULL)
		return;

	free_children (path);

	if (path->uri)
		gnome_vfs_uri_unref (path->uri);
	if (path->pixbuf)
		gdk_pixbuf_unref (path->pixbuf);
	if (path->info)
		gnome_vfs_file_info_unref (path->info);

	g_chunk_free (path, node_chunk);
}

static GnomeVFSTreeModelPath *
new_path (GnomeVFSTreeModelPath *parent)
{
	GnomeVFSTreeModelPath *path;

	path = g_chunk_new0 (GnomeVFSTreeModelPath, node_chunk);

	path->parent = parent;
	path->num_children = -1;
	path->children = NULL;
	path->position = -1;

	path->uri = NULL;
	path->info = NULL;
	path->pixbuf = NULL;

	return path;
}

static GdkPixbuf *
get_icon_from_file_info (GnomeVFSFileInfo *info, GnomeVFSURI *uri)
{
	char *uri_text;
	NautilusFile *file;
	GdkPixbuf *pixbuf;
	gboolean optimize_for_anti_aliasing = TRUE;

	uri_text = gnome_vfs_uri_to_string (uri, GNOME_VFS_URI_HIDE_NONE);
	file = nautilus_file_get (uri_text);
	pixbuf = nautilus_icon_factory_get_pixbuf_for_file (file, '\0', ROW_HEIGHT - 2, optimize_for_anti_aliasing);
	g_free (uri_text);
	return pixbuf;
}

typedef struct {
	GnomeVFSTreeModel *model;
	GnomeVFSTreeModelPath *parent;
} DumbStruct;

/* when a directory load is started (asynchronously) a list of
   GnomeVFSFileInfos is periodically pumped into this function
   representing files in the directory.  When the listing is over
   GnomeVFS sets the result to GNOME_VFS_ERROR_EOF */
static void
load_directory_callback (GnomeVFSAsyncHandle *handle,
			 GnomeVFSResult result,
			 GList *list,
			 guint entries_read,
			 gpointer callback_data)
{
	DumbStruct *dumb = callback_data;
	GnomeVFSTreeModel *tree_model = dumb->model;
	GnomeVFSTreeModelPath *parent = dumb->parent;
	gint old_count = parent->num_children;
	gint new_count = old_count + entries_read;
	int i;

	parent->children = g_renew (GnomeVFSTreeModelPath *, parent->children, new_count);

	for (i = old_count; i < new_count; i++) {
		GnomeVFSFileInfo *info = list->data;

		e_tree_model_pre_change (E_TREE_MODEL(tree_model));

		parent->children[i] = new_path (parent);
		parent->children[i]->info = info;
		parent->children[i]->uri = gnome_vfs_uri_append_file_name (parent->uri, info->name);
		parent->children[i]->pixbuf = get_icon_from_file_info (info, parent->children[i]->uri);
		parent->children[i]->position = i;
		gnome_vfs_file_info_ref (info);

		list = list->next;

		parent->num_children ++;
		e_tree_model_node_inserted (E_TREE_MODEL(tree_model), parent, parent->children[i]);
	}

	if (result == GNOME_VFS_ERROR_EOF) {
		g_free (dumb);
	}
}

static void
generate_children (GnomeVFSTreeModel *gvtm,
		   GnomeVFSTreeModelPath *parent)
{
	if (parent->uri) {
		GnomeVFSAsyncHandle *handle;
		GnomeVFSFileInfoOptions info_options =
			GNOME_VFS_FILE_INFO_GET_MIME_TYPE;
		GnomeVFSDirectoryFilterType filter_type =
			GNOME_VFS_DIRECTORY_FILTER_NONE;
		GnomeVFSDirectoryFilterOptions filter_options =
			GNOME_VFS_DIRECTORY_FILTER_NODOTFILES |
			GNOME_VFS_DIRECTORY_FILTER_NOBACKUPFILES;
		guint items_per_notification = 10;

		DumbStruct *dumb;


		dumb = g_new (DumbStruct, 1);
		dumb->model = gvtm;
		dumb->parent = parent;

		parent->num_children = 0;

		gnome_vfs_async_load_directory_uri (&handle, parent->uri, info_options,
						    filter_type, filter_options, "*",
						    items_per_notification,
						    &load_directory_callback,
						    dumb);
	}
}


/* virtual methods */

static void
gvtm_destroy (GtkObject *object)
{
	GnomeVFSTreeModel *gvtm = GNOME_VFS_TREE_MODEL (object);
	GnomeVFSTreeModelPriv *priv = gvtm->priv;

	/* FIXME lots of stuff to free here */

	free_path (priv->root);

	gnome_vfs_uri_unref(priv->uri);

	g_free (priv);

	GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static ETreePath
gvtm_get_root (ETreeModel *etm)
{
	GnomeVFSTreeModelPriv *priv = GNOME_VFS_TREE_MODEL (etm)->priv;
	if (priv->root == NULL) {
		priv->root = new_path (NULL);
		priv->root->uri = priv->uri;
		if (priv->root->uri)
			gnome_vfs_uri_ref(priv->root->uri);
	}
	if (priv->root && priv->root->num_children == -1) {
		generate_children (GNOME_VFS_TREE_MODEL (etm), priv->root);
	}

	return priv->root;
}

static ETreePath
gvtm_get_parent (ETreeModel *etm, ETreePath node)
{
	GnomeVFSTreeModelPath *path = node;
	return path->parent;
}

static ETreePath
gvtm_get_first_child (ETreeModel *etm, ETreePath node)
{
	GnomeVFSTreeModelPath *path = node;
	GnomeVFSTreeModel *gvtm = GNOME_VFS_TREE_MODEL (etm);

	if (path->num_children == -1)
		generate_children (gvtm, path);

	if (path->num_children > 0)
		return path->children[0];
	else
		return NULL;
}

static ETreePath
gvtm_get_last_child (ETreeModel *etm, ETreePath node)
{
	GnomeVFSTreeModelPath *path = node;
	GnomeVFSTreeModel *gvtm = GNOME_VFS_TREE_MODEL (etm);

	if (path->num_children == -1)
		generate_children (gvtm, path);

	if (path->num_children > 0)
		return path->children[path->num_children - 1];
	else
		return NULL;
}

static ETreePath
gvtm_get_next (ETreeModel *etm, ETreePath node)
{
	GnomeVFSTreeModelPath *path = node;
	GnomeVFSTreeModelPath *parent = path->parent;
	if (parent) {
		if (parent->num_children > path->position + 1)
			return parent->children[path->position + 1];
		else
			return NULL;
	} else
		  return NULL;
}

static ETreePath
gvtm_get_prev (ETreeModel *etm, ETreePath node)
{
	GnomeVFSTreeModelPath *path = node;
	GnomeVFSTreeModelPath *parent = path->parent;
	if (parent) {
		if (path->position - 1 >= 0)
			return parent->children[path->position - 1];
		else
			return NULL;
	} else
		  return NULL;
}

static gboolean
gvtm_is_root (ETreeModel *etm, ETreePath node)
{
	GnomeVFSTreeModel *gvtm = GNOME_VFS_TREE_MODEL (etm);

	return gvtm->priv->root == node;
}

static gboolean
gvtm_is_expandable (ETreeModel *etm, ETreePath node)
{
 	GnomeVFSTreeModelPath *path = node;
	GnomeVFSTreeModel *gvtm = GNOME_VFS_TREE_MODEL (etm);

	if (path->num_children == -1) {
		generate_children (gvtm, node);
	}

	return path->num_children != 0;
}

static guint
gvtm_get_children (ETreeModel *etm, ETreePath node, ETreePath **nodes)
{
	GnomeVFSTreeModelPath *path = node;
	guint n_children;

	if (path->num_children == -1) {
		generate_children (GNOME_VFS_TREE_MODEL (etm), node);
	}

	n_children = path->num_children;

	if (nodes) {
		int i;

		 (*nodes) = g_malloc (sizeof (ETreePath) * n_children);
		for (i = 0; i < n_children; i ++) {
			 (*nodes)[i] = path->children[i];
		}
	}

	return n_children;
}

static guint
gvtm_depth (ETreeModel *etm, ETreePath node)
{
	int i;
	GnomeVFSTreeModelPath *path = node;

	path = path->parent;

	for (i = 0; path; i++)
		path = path->parent;

	return i;
}

static GdkPixbuf *
gvtm_icon_at (ETreeModel *etm, ETreePath node)
{
	GnomeVFSTreeModelPath *path = node;

	return path->pixbuf;
}

static gboolean
gvtm_get_expanded_default (ETreeModel *etm)
{
	return FALSE;
}

static gint
gvtm_column_count (ETreeModel *etm)
{
	return LAST_COL;
}


static gboolean
gvtm_has_save_id (ETreeModel *etm)
{
	return TRUE;
}

static gchar *
gvtm_get_save_id (ETreeModel *etm, ETreePath node)
{
	GnomeVFSTreeModelPath *path = node;

	if (path->uri)
		return gnome_vfs_uri_to_string (path->uri, GNOME_VFS_URI_HIDE_NONE);
	else
		return g_strdup("root");
}


static void *
gvtm_value_at (ETreeModel *etm, ETreePath node, int col)
{
	GnomeVFSTreeModelPath *path = node;

	switch (col) {
	case COL_FILE_NAME:
		/* FIXME: Unescape? */
		return path->info->name;
	case COL_DATE_MODIFIED:
		if (path->info)
			return GINT_TO_POINTER (path->info->mtime);
		else
			return 0;
	default:
		//g_assert_not_reached ();
	}
	return NULL;
}

static void
gvtm_set_value_at (ETreeModel *etm, ETreePath node, int col, const void *val)
{
	switch (col) {
	case COL_FILE_NAME:
		g_assert_not_reached ();
	case COL_DATE_MODIFIED:
		g_assert_not_reached ();
	default:
		g_assert_not_reached ();
	}
}

static gboolean
gvtm_is_editable (ETreeModel *etm, ETreePath node, int col)
{
	return FALSE;
}


/* The default for gvtm_duplicate_value is to return the raw value. */
static void *
gvtm_duplicate_value (ETreeModel *etm, int col, const void *value)
{
	switch (col) {
	case COL_FILE_NAME:
		return g_strdup (value);
	case COL_DATE_MODIFIED:
		return (void *) value;
	default:
		g_assert_not_reached ();
	}
	return NULL;
}

static void
gvtm_free_value (ETreeModel *etm, int col, void *value)
{
	switch (col) {
	case COL_FILE_NAME:
		g_free (value);
		break;
	case COL_DATE_MODIFIED:
		break;
	default:
		g_assert_not_reached ();
	}
}

static void *
gvtm_initialize_value (ETreeModel *etm, int col)
{
	switch (col) {
	case COL_FILE_NAME:
		return g_strdup ("");
	case COL_DATE_MODIFIED:
		return GINT_TO_POINTER (0);
	default:
		g_assert_not_reached ();
	}
	return NULL;
}

static gboolean
gvtm_value_is_empty (ETreeModel *etm, int col, const void *value)
{
	switch (col) {
	case COL_FILE_NAME:
		return (value == NULL || *(char *)value == 0);
	case COL_DATE_MODIFIED:
		return ! GPOINTER_TO_INT (value);
	default:
		g_assert_not_reached ();
	}
	return FALSE;
}

static char *
gvtm_value_to_string (ETreeModel *etm, int col, const void *value)
{
	switch (col) {
	case COL_FILE_NAME:
		return g_strdup (value);
	case COL_DATE_MODIFIED:
		return filter_date ((time_t) value);
	default:
		g_assert_not_reached ();
	}
	return NULL;
}





/* Initialization and creation */

static void
gnome_vfs_tree_model_class_init (GtkObjectClass *klass)
{
	ETreeModelClass *tree_class      = (ETreeModelClass *) klass;

	parent_class                     = gtk_type_class (PARENT_TYPE);

	node_chunk                       = g_mem_chunk_create
		 (GnomeVFSTreeModelPath,
		 TREEPATH_CHUNK_AREA_SIZE,
		 G_ALLOC_AND_FREE);
	
	klass->destroy                   = gvtm_destroy;

	tree_class->get_root             = gvtm_get_root;
	tree_class->get_parent           = gvtm_get_parent;
	tree_class->get_first_child      = gvtm_get_first_child;
	tree_class->get_last_child       = gvtm_get_last_child;
	tree_class->get_prev             = gvtm_get_prev;
	tree_class->get_next             = gvtm_get_next;

	tree_class->is_root              = gvtm_is_root;
	tree_class->is_expandable        = gvtm_is_expandable;
	tree_class->get_children         = gvtm_get_children;
	tree_class->depth                = gvtm_depth;

	tree_class->icon_at              = gvtm_icon_at;

	tree_class->get_expanded_default = gvtm_get_expanded_default;
	tree_class->column_count         = gvtm_column_count;

	tree_class->has_save_id          = gvtm_has_save_id;
	tree_class->get_save_id          = gvtm_get_save_id;




	tree_class->value_at             = gvtm_value_at;
	tree_class->set_value_at         = gvtm_set_value_at;
	tree_class->is_editable          = gvtm_is_editable;

	tree_class->duplicate_value      = gvtm_duplicate_value;
	tree_class->free_value           = gvtm_free_value;
	tree_class->initialize_value     = gvtm_initialize_value;
	tree_class->value_is_empty       = gvtm_value_is_empty;
	tree_class->value_to_string      = gvtm_value_to_string;
}

static void
gnome_vfs_tree_model_init (GtkObject *object)
{
	GnomeVFSTreeModel *gvtm = (GnomeVFSTreeModel *)object;

	GnomeVFSTreeModelPriv *priv;

	priv       = g_new0 (GnomeVFSTreeModelPriv, 1);
	gvtm->priv = priv;

	priv->root = NULL;
}

E_MAKE_TYPE (gnome_vfs_tree_model,
	     "GnomeVFSTreeModel",
	     GnomeVFSTreeModel,
	     gnome_vfs_tree_model_class_init,
	     gnome_vfs_tree_model_init,
	     PARENT_TYPE)

/**
 * gnome_vfs_tree_model_construct:
 * @etree: 
 * 
 * 
 **/
void
gnome_vfs_tree_model_construct (GnomeVFSTreeModel *gvtm, GnomeVFSURI *uri)
{
	gnome_vfs_tree_model_change_uri(gvtm, uri);
}

/**
 * gnome_vfs_tree_model_new
 *
 * FIXME docs here.
 *
 * return values: a newly constructed GnomeVFSTreeModel.
 */
GnomeVFSTreeModel *
gnome_vfs_tree_model_new (GnomeVFSURI *uri)
{
	GnomeVFSTreeModel *gvtm;

	gvtm = gtk_type_new (gnome_vfs_tree_model_get_type ());

	gnome_vfs_tree_model_construct (gvtm, uri);

	return gvtm;
}

void
gnome_vfs_tree_model_change_uri (GnomeVFSTreeModel *model, GnomeVFSURI *uri)
{
	e_tree_model_pre_change(E_TREE_MODEL(model));
	if (model->priv->root != NULL) {
		free_path(model->priv->root);
		model->priv->root = NULL;
	}

	if (model->priv->uri)
		gnome_vfs_uri_unref(model->priv->uri);

	model->priv->uri = uri;
	if (uri)
		gnome_vfs_uri_ref (uri);

	e_tree_model_node_changed(E_TREE_MODEL(model), gvtm_get_root(E_TREE_MODEL(model)));
}

GnomeVFSURI *
gnome_vfs_tree_model_node_get_uri (GnomeVFSTreeModel *model,
				   ETreePath          node)
{
	GnomeVFSTreeModelPath *path = node;
	if (path->uri)
		gnome_vfs_uri_ref(path->uri);
	return path->uri;
}
