#ifndef FILE_SELECTOR_H__
#define FILE_SELECTOR_H__

#include <libgnomevfs/gnome-vfs.h>

typedef enum {
  FILE_SELECTOR_OPEN,
  FILE_SELECTOR_SAVE
} FileSelectorType;

void file_selector_select (FileSelectorType type, char *mime_types[]);

#endif
