/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * gnome-vfs-tree-model.h: a Tree Model implementation that the
 * programmer.
 *
 * Author:
 *   Chris Lahey <clahey@ximian.com>
 *   Seth Nickell <snickell@stanford.edu>
 *
 * Adapted from the gtree code and ETableModel.
 *
 * (C) 2000, 2001 Ximian, Inc.
 */

#ifndef _GNOME_VFS_TREE_MODEL_H_
#define _GNOME_VFS_TREE_MODEL_H_

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gal/e-table/e-tree-model.h>
#include <libgnomevfs/gnome-vfs.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GNOME_VFS_TREE_MODEL_TYPE        (gnome_vfs_tree_model_get_type ())
#define GNOME_VFS_TREE_MODEL(o)          (GTK_CHECK_CAST ((o), GNOME_VFS_TREE_MODEL_TYPE, GnomeVFSTreeModel))
#define GNOME_VFS_TREE_MODEL_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GNOME_VFS_TREE_MODEL_TYPE, GnomeVFSTreeModelClass))
#define GNOME_VFS_IS_TREE_MODEL(o)       (GTK_CHECK_TYPE ((o), GNOME_VFS_TREE_MODEL_TYPE))
#define GNOME_VFS_IS_TREE_MODEL_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GNOME_VFS_TREE_MODEL_TYPE))

typedef struct GnomeVFSTreeModel GnomeVFSTreeModel;
typedef struct GnomeVFSTreeModelPriv GnomeVFSTreeModelPriv;
typedef struct GnomeVFSTreeModelClass GnomeVFSTreeModelClass;

struct GnomeVFSTreeModel {
	ETreeModel base;

	GnomeVFSTreeModelPriv *priv;
};

struct GnomeVFSTreeModelClass {
	ETreeModelClass parent_class;
};


GtkType            gnome_vfs_tree_model_get_type     (void);
void               gnome_vfs_tree_model_construct    (GnomeVFSTreeModel *etree,
						      GnomeVFSURI       *uri);
GnomeVFSTreeModel *gnome_vfs_tree_model_new          (GnomeVFSURI       *uri);

void               gnome_vfs_tree_model_change_uri   (GnomeVFSTreeModel *model,
						      GnomeVFSURI       *uri);
GnomeVFSURI       *gnome_vfs_tree_model_node_get_uri (GnomeVFSTreeModel *model,
						      ETreePath          path);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _GNOME_VFS_TREE_MODEL_H */
